//#include <strtok.h>
#include <cssshamir.h>
//#include <d_string.h>
 
#include "M5Display.h"
#define TEXT "aA MWyz~12"
#include "Free_Fonts.h"

#include <nist256p1.h>
#include <ripemd160.h>
#include <address.h>
#include <segwit_addr.h>
#include <types.pb.h>
#include <debug.h>
#include <script.h>
#include <blake2b.h>
#include <base58.h>
#include <blake2s.h>
#include <sha2.h>
#include <sha3.h>
#include <check_mem.h>
#include <bip32.h>
#include <ecdsa.h>
#include <pbkdf2.h>
#include <options.h>
#include <crypto.h>
#include <base32.h>
#include <bip39_english.h>
#include <bip39.h>
#include <hasher.h>
#include <sha2.h>
#include <hmac.h>
#include <pb_common.h>
#include <bip39.h>
#include <gettext.h>
#include <macros.h>
#include <rc4.h>
#include <util.h>
#include <options.h>
#include <signing.h>
#include <rfc6979.h>
#include <curves.h>
#include <pb.h>
#include <blake2_common.h>
#include <blake256.h>
#include <coins_count.h>
#include <transaction.h>
#include <pb_encode.h>
#include <bignum.h>
#include <secp256k1.h>
#include <pb_decode.h>
#include <shamir.h>


#define VERSION_PUBLIC 0x0488b21e
#define VERSION_PRIVATE 0x0488ade4

#define DECRED_VERSION_PUBLIC 0x02fda926
#define DECRED_VERSION_PRIVATE 0x02fda4e8

#define FROMHEX_MAXLEN 512

#define SHAMIR_MAX_COUNT 16




M5Display Lcd = M5Display();

const uint8_t *fromhex(const char *str) {
  static uint8_t buf[FROMHEX_MAXLEN];
  size_t len = strlen(str) / 2;
  if (len > FROMHEX_MAXLEN) len = FROMHEX_MAXLEN;
  for (size_t i = 0; i < len; i++) {
    uint8_t c = 0;
    if (str[i * 2] >= '0' && str[i * 2] <= '9') c += (str[i * 2] - '0') << 4;
    if ((str[i * 2] & ~0x20) >= 'A' && (str[i * 2] & ~0x20) <= 'F')
      c += (10 + (str[i * 2] & ~0x20) - 'A') << 4;
    if (str[i * 2 + 1] >= '0' && str[i * 2 + 1] <= '9')
      c += (str[i * 2 + 1] - '0');
    if ((str[i * 2 + 1] & ~0x20) >= 'A' && (str[i * 2 + 1] & ~0x20) <= 'F')
      c += (10 + (str[i * 2 + 1] & ~0x20) - 'A');
    buf[i] = c;
  }
  return buf;
}

void displayData( const uint8_t *rspPtr, uint8_t bufLen ) {
  const uint8_t *bufPtr = rspPtr;
  // Display Lcd number and add to LcdNum buffer
  for (int i = 0; i < bufLen; i++ ) {
    Lcd.print("0x");
    if ( bufPtr[i] < 16 ) Lcd.print(F("0"));
    Lcd.print(bufPtr[i], HEX);
    Lcd.print(",");
  }
  Lcd.println();
}

// this function is from rmxwallet.io, originally added to rmxwallet by m2049r 
#define FROMHEX_MAXLEN 512
#define TOHEX_MAXLEN (2*FROMHEX_MAXLEN)
const char *tohexLE(const uint8_t * in, size_t inlen) {
  static char buf[TOHEX_MAXLEN+1];
  const char * hex = "0123456789abcdef";
  size_t len = inlen * 2;
  if (len > TOHEX_MAXLEN) len = TOHEX_MAXLEN;
  for (size_t i = 0; i < len/2; i++) {
    buf[2*i+0] = hex[(in[i]>>4) & 0xF];
    buf[2*i+1] = hex[ in[i]     & 0xF];
  }
buf[len] = 0;
return buf;
}//esp_random();


void setup() {

  //M5Display Lcd = M5Display();

  Lcd.begin();
  delay(1000);

   // text print
  Lcd.fillScreen(BLACK);
  Lcd.setCursor(10, 10);
  Lcd.setTextColor(WHITE);
  Lcd.setTextSize(1);

  /*M5.Lcd.setTextColor(TFT_WHITE, TFT_BLACK);
  M5.Lcd.fillScreen(TFT_BLACK);
  M5.Lcd.setFreeFont(FF18);*/
  
  Lcd.println("     WORDS TO BYTES:\r\n");
  
  const char mnemonic[] = "explain eager alter north limit wise inquiry between peace twelve fiction leaf";
  const char passphrase[] = "RIATTED";
  Lcd.println(mnemonic);
  Lcd.print("\r\n");


  //const char *passphrase = mnemo;
  int mnemonic_len = strlen(mnemonic);
  //Lcd.println(String("StringLen of Mnemonic: ") + strlen(mnemonic));
  const char salt[] = "electrum";
  int salt_len = strlen(salt);
  //memcpy(salt, "electrum", 8);
  //probably checking if words are consistent?
  int res = mnemonic_check(mnemonic);
 


  // Now making bytes out of seed mnemonic wordlist
  uint8_t seed[32];
  mnemonic_to_seed(mnemonic, passphrase, seed, NULL);

  
  Lcd.print(tohexLE(seed, 32));
  //displayData( seed, 32 );

  //displayData(seed, 32);
  //displayData(result, 32);

  /**  Now another end, bytes to mnemonic words**/
  Lcd.println("\r\n\r\n     BYTES TO WORDS:\r\n\r\n");

  // getting random bits into the randombytes field
  uint8_t randombytes[32];
  for(uint8_t i = 0; i<(32/4); i++) {
    uint32_t a = esp_random();
    memcpy((uint8_t*)&randombytes[i*4], (uint8_t*)&a, 4);
  }

  // print them as a hex string
  Lcd.println("Random bytes:\r\n");
  Lcd.print(tohexLE(randombytes, 32));

  // making words out of that
  char mnemonics[256]; //fixme length?
  Lcd.print("\r\n\r\n");
  Lcd.print(mnemonic_from_data(randombytes, 32));
}

void loop() {
}
